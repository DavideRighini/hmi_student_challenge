from operator import itemgetter #for min in list
import time,serial,sys,threading,getopt,os,io
from sys import stdin
from datetime import date,time,datetime,timedelta
from time import sleep

#serial port configuration


def character(k):
	return k.decode('latin1')

try:
    from serial.tools.list_ports import comports
except ImportError:
	comports = None
	sys.exit(1)

if comports:
	sys.stderr.write('\n--- Available ports:\n')
	for port, desc, hwid in sorted(comports()):
		#~ sys.stderr.write('--- %-20s %s [%s]\n' % (port, desc, hwid))
		sys.stderr.write('--- %-20s %s\n' % (port, desc))
	com_port_key = int(stdin.readline())
	com_port_key_str="COM" + str(com_port_key)
	print(com_port_key_str)	
	
try:			
	s= serial.Serial(com_port_key_str, 9600, rtscts=False, xonxoff=False, dsrdtr=False, timeout=None, write_timeout=None)
	sio=io.TextIOWrapper(io.BufferedRWPair(s, s))
	print("info porta seriale attiva:")
	print(s)                           # print serial config
except serial.SerialException as e:
	sys.stderr.write("could not open port")
	sys.exit(1)

	
#console 	


CR = serial.to_bytes([13])
CRLF = serial.to_bytes([13, 10])

X00 = serial.to_bytes([0])
X0E = serial.to_bytes([0x0e])

LF = serial.to_bytes([10]) #newline
EXITCHARCTER = character(serial.to_bytes([0x03]))  # CTRL+C
NEWLINE_CONVERISON_MAP = (LF, CR, CRLF)
newline = NEWLINE_CONVERISON_MAP[2]

#variabile definition

global packet_str
packet_str=""
global packet_counter_num
packet_counter_num=0
global packet_counter_index
packet_counter_index=0
global packet_sent_total
packet_sent_total=100 
global end_couter_index
end_couter_index=0
global inz_couter_index
inz_couter_index=0
global ch_count_error
ch_count_error = 0

# variables for LoRa packet structure

B_ID =1; #bridge id
S_ID =1; #sensor id 
preambolo = str(B_ID) + "-" + str(S_ID)
end_str="\n"

global console
if os.name == 'nt':
    import msvcrt
    class Console(object):
        def __init__(self):
            pass

        def setup(self):
            pass    # Do nothing for 'nt'

        def cleanup(self):
            pass    # Do nothing for 'nt'

        def getkey(self):
            while True:
                z = msvcrt.getch()
                if z == X00 or z == X0E:    # functions keys, ignore
                    msvcrt.getch()
                else:
                    if z == CR:
                        return LF
                    return z

    console = Console()

def create_packet_list(file_name):
	#leggi il file data_to_send.txt
	with open(file_name) as f:
		content = f.readlines()
	# you may also want to remove whitespace characters like `\n` at the end of each line
	content = [x.strip(chr(13)) for x in content] 
	# content[n] is the list of packets
	return content
	
def invia_tabella():
	global packet_sent_total
	pachet_list = create_packet_list('dati_seriale_rand.txt')
	if (len(pachet_list)-1) < packet_sent_total:
		packet_sent_total = len(pachet_list)-1
		
	for j in range(0,packet_sent_total):
		#try:
			
		data= preambolo + pachet_list[j] + end_str
		print(data)
		#scrivi i dati della tabella sulla seriale una riga alla volta
		serial_alive =True
		bytesToRead=1
		cont=0
		i=0
		#function ord() would get the int value of the char. 
		#convert back the number use function chr() 
		#print("\ninvio dati:\n")
		sio.write(str(data))
		#sio.flush()				
		#sys.stdout.flush()
		
		#aspetta l'ok ogni riga
		#risposta()
		#sys.stdout.write('.')
		#except (SystemExit, KeyboardInterrupt, GeneratorExit, Exception):
		#	print("No answer.\n")
		#	sys.stdout.flush()			
		
		#sleep(0.0001)
	sio.flush()				
	sys.stdout.flush()
	return		

def act_time_row():
	d = datetime.now()
	return d
	
def print_act_time(d):
	h=d.hour
	m=d.minute
	sec=d.second
	usec=d.microsecond
	gg=d.day
	yy=d.year
	mm=d.month
	dd= datetime.today()
	g = datetime.isoweekday(dd)
	data="H_" + str(h) + "--" + str(m) + "--" + str(sec) + "_" + str(usec) + "00000000000000000000000000000000000000000"
	return str(data)

def print_deltatime(d):
	sec=d.seconds
	usec=d.microseconds
	data="HD:" + str(sec) + "." + str(usec)
	return str(data)

	
def act_time():
	d = act_time_row()
	time_str = print_act_time(d)
	data = preambolo + time_str + end_str
	print(data)
	sio.write(str(data))
	sio.flush()
	return

def risposta():
	#reeds the message answer
	cont=0
	print("\ncontrolla risposta...   ")
	while (cont<=1):
		bytesToRead = s.inWaiting()
		ch = character(s.read(1))

		if ch=='o':
			cont=cont+1
		elif ch=='k' and cont==1:
			print("Data sent correctly")
			break
		else:
			cont=0
	return

	
def packet_counter(ch):
	result = 0
	#return the number of the arrived packets
	global packet_str
	global packet_counter_num
	global packet_counter_index
	if packet_counter_index==0 and ch!='1':
		packet_counter_index=0
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==2 and ch=='1':
		packet_counter_index=3
		packet_counter_num = packet_counter_num + 1
		if packet_counter_num != 1:
			pass
			#controlla pachetto ricevuto
			#conta errori nel pacchetto
			#print("\nReceived packet message: " + packet_str + "\n")
		packet_str=""
		result = 1
	elif packet_counter_index==1 and ch=='1':
		packet_counter_index=1
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==0 and ch=='1':
		packet_counter_index=1
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==1 and ch=='-':
		packet_counter_index=2
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==1 and ch!='-' and ch!='1':
		packet_counter_index=0
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==2 and ch!='1':
		packet_counter_index=0
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==3 and ch=='1':
		packet_counter_index=1
		packet_str=packet_str+ch
		result = 0
	elif packet_counter_index==3 and ch!='1':
		packet_counter_index=0
		packet_str=packet_str+ch
		result = 0
	else:
		print("error\n")
		sys.exit(1)
	return result

def end_packet_finder(ch):
	global end_couter_index
	result=0
	if end_couter_index==3 and ch=='e':
		end_couter_index=1
		result=0
	elif end_couter_index==3 and ch!='e':
		end_couter_index=0
		result=0
	elif end_couter_index==2 and ch=='d':
		end_couter_index=3
		result=1
	elif end_couter_index==2 and ch=='e':
		end_couter_index=1
		result=0
	elif end_couter_index==2 and (ch!='n' or ch!='e'):
		end_couter_index=0
		result=0		
	elif end_couter_index==1 and ch=='n':
		end_couter_index=2
		result=0
	elif end_couter_index==1 and ch=='e':
		end_couter_index=1
		result=0
	elif end_couter_index==1 and (ch!='n' or ch!='e'):
		end_couter_index=0
		result=0
	elif end_couter_index==0 and ch=='e':
		end_couter_index=1
		result=0
	elif end_couter_index==0 and ch!='e':
		end_couter_index=0
		result=0
	return result
	
def inz_packet_finder(ch):
	global inz_couter_index
	result=0
	if inz_couter_index==3 and ch=='i':
		inz_couter_index=1
		result=0
	elif inz_couter_index==3 and ch!='i':
		inz_couter_index=0
		result=0
	elif inz_couter_index==2 and ch=='z':
		inz_couter_index=3
		result=1
	elif inz_couter_index==2 and ch=='i':
		inz_couter_index=1
		result=0
	elif inz_couter_index==2 and (ch!='n' or ch!='i'):
		inz_couter_index=0
		result=0		
	elif inz_couter_index==1 and ch=='n':
		inz_couter_index=2
		result=0
	elif inz_couter_index==1 and ch=='i':
		inz_couter_index=1
		result=0
	elif inz_couter_index==1 and (ch!='n' or ch!='i'):
		inz_couter_index=0
		result=0
	elif inz_couter_index==0 and ch=='i':
		inz_couter_index=1
		result=0
	elif inz_couter_index==0 and ch!='i':
		inz_couter_index=0
		result=0
	return result

def test_BER():
	global str_time_start_of_serial_reader
	
	with open('dati_seriale_rand.txt') as f:
		ref_content = f.readlines()
	ref_content = [x.strip('\n') for x in ref_content] 

	with open('dati_seriale ' + str_time_start_of_serial_reader + ".txt") as f:
		con_content = f.readlines()
	con_content = [x.strip('\n') for x in con_content]

	for i in range(0,len(con_content)-1):
		if "***" in con_content[i]:
			end_file_index=i 
		else:	
			pass


	#add start code to packet		
	for i in range(0,len(ref_content)-1):
		ref_content[i] = "1-1" + ref_content[i]
		#print(ref_content[i])


	ch_error_counter = [[0 for x in range(end_file_index)] for y in range(len(ref_content)-1)]

	for i in range(0,end_file_index):
		str_con_content_ch=list(con_content[i])
		str_con_content = ''.join(format(ord(x), 'b') for x in str_con_content_ch)
		for j in range(0,len(ref_content)-1):

			str_ref_content_ch =list(ref_content[j])
			str_ref_content = ''.join(format(ord(x), 'b') for x in str_ref_content_ch)
			if len(str_con_content)!=len(str_ref_content):
				ch_error_counter[j][i]=len(str_con_content)

			else:
				for k in range(0,len(str_con_content)):
					if str_ref_content[k]!=str_con_content[k]:				
						ch_error_counter[j][i]=ch_error_counter[j][i]+1

	tot_bit_errors=0
	for i in range(0,end_file_index):
		act_min=ch_error_counter[1][i]
		index_min=1
		for j in range(0,len(ref_content)-1):
			if act_min>ch_error_counter[j][i]:
				act_min=ch_error_counter[j][i]
				index_min=j
			#print("Index i: " + str(i) + ", Index j: " + str(j) +", ch errors: " + str(ch_error_counter[j][i]))
		print("****Index: " + str(index_min) + ", ch errors: " + str(ch_error_counter[index_min][i]))
		tot_bit_errors = tot_bit_errors + ch_error_counter[index_min][i]

	print(tot_bit_errors)
	
	return tot_bit_errors/(end_file_index*64*8)

def print_BER(ber_value):
	
	global str_time_start_of_serial_reader
	text_file = open('dati_seriale '+ str_time_start_of_serial_reader +'.txt', 'a+')
	info_BER_str = "BER: " + str(ber_value) + "\n"
	print(info_BER_str)
	text_file.write(info_BER_str)
	text_file.flush()
	return

def read_serial():
	#read serial port^
	time_start_of_serial_reader = act_time_row()
	global str_time_start_of_serial_reader
	str_time_start_of_serial_reader = print_act_time(time_start_of_serial_reader)
	text_file = open('dati_seriale '+ str_time_start_of_serial_reader +'.txt', 'a+')
	
	
	global time_first_character_arrived
	global time_last_character_arrived
	
	#timer inizializzation
	time_first_character_arrived = act_time_row()
	time_last_character_arrived = act_time_row()
	
	#counters inizializzation
	arrived_string=""
	cont=0
	cont_characters=0;
	cont_characters_real=0;
	flag_ini=0
	cont_new_line=0
	try:
	
		while (1):
			cont=cont+1
			
			ch = character(s.read(1))
			if ch!=chr(0):
				sys.stdout.write(ch)
				arrived_string=arrived_string+ch
				
				#first character
				if cont==1:
					time_first_character_arrived = act_time_row()
					time_first_character_arrived_str = print_act_time(time_first_character_arrived)
				else:
					time_last_character_arrived = act_time_row()
					time_last_character_arrived_str = print_act_time(time_last_character_arrived)
					
				#flush file and screen every 100 characters
				if cont%100==0:
					sys.stdout.flush()
				
				#count new line
				if ch == '\n':
					cont_new_line=cont_new_line+1
					time_last_character_arrived = act_time_row()
				#count arrived characters
				if ch != '':
					cont_characters=cont_characters+1
				#count arrived characters after word "ini"
				if ch != '' and flag_ini==1:
					cont_characters_real=cont_characters_real+1
				
				#find packet that contains "inz"
				result = inz_packet_finder(ch)
				if result ==1:
					print("\nInz packet arrived...\n")
					
				#find packet that contains "end"
				result = end_packet_finder(ch)
				if result ==1:
					print("\nLast character arrived...\n")
					break
				#find packets
				result = packet_counter(ch)
				if result ==1:
					if flag_ini==0:
						print("\nFirst packet arrived\n")
						flag_ini=1
	except (SystemExit, KeyboardInterrupt, GeneratorExit, Exception):
		print("Communication error... exit..\n")
		sys.stdout.flush()
	
	text_file.write(arrived_string)
	text_file.write("\n*****\n")		
	#print to file communication information
	info_characters_num_str = "Total arrived characters: " + str(cont_characters)+ "\n"
	info_characters_real_num_str = "Arrived characters after (ini): " + str(cont_characters_real)+ "\n"
	delta_time_communication = time_last_character_arrived - time_first_character_arrived
	delta_time_communication_str = print_deltatime(delta_time_communication)
	info_delta_time_communication_str = "Delta time communication: " + delta_time_communication_str + "\n"
	bit_per_second=8*cont/(delta_time_communication.seconds+0.000001*delta_time_communication.microseconds)
	info_bit_per_second = "Bit per second: " + str(bit_per_second) + "\n"
	info_packet_counter_num= "Arrived packets: " + str(packet_counter_num) + "\n"
	info_count_new_line_str = "Arrived new line: " + str(cont_new_line) + "\n"
	PER=packet_counter_num/packet_sent_total
	info_PER_str = "Packet error rate PER: " + str(PER) + "\n"
	print(info_characters_num_str)
	print(info_characters_real_num_str)
	print(info_delta_time_communication_str)
	print(info_bit_per_second)
	print(info_packet_counter_num)
	print(info_count_new_line_str)
	print(info_PER_str)
	text_file.write("\n\n")
	text_file.write(info_characters_num_str)
	text_file.write(info_delta_time_communication_str)
	text_file.write(info_bit_per_second)
	text_file.write(info_packet_counter_num)
	text_file.write(info_count_new_line_str)
	text_file.write(info_PER_str)
	text_file.flush()
	sys.stdout.flush()
	return


print('*************************************************************************')
print('                     VIRTUAL SENSOR & SERIAL HMI                         ')
print('*************************************************************************')
print('\n')

print('Settings:')
print('1: Send data to Lora modem')
print('2: Save serial log')

scelta = int(stdin.readline())

if scelta==1:
	invia_tabella()
elif scelta==2:
	read_serial()
	try:
		ber_percent = test_BER()
		print_BER(ber_percent)
	except Exception:
		print("Can't calculate BER.\n")
sleep(10)
s.close()
sys.exit()